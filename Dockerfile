FROM golang:1.12-alpine as base
RUN apk --no-cache add git ca-certificates g++
WORKDIR /repos/nanoleaf-service
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/nanoleaf-service
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o nanoleaf-service

FROM scratch as release
COPY --from=builder /repos/nanoleaf-service/nanoleaf-service /nanoleaf-service
EXPOSE 8080
ENTRYPOINT ["/nanoleaf-service"]
