package commands

import (
	"errors"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

var NanoCommand = lib.Action{
	Proc:    "command.nano",
	Handler: nano,
}

type nanoInput struct {
	Colors wamp.List
}

func nano(invocation *wamp.Invocation) client.InvokeResult {
	input, err := getNanoInput(invocation.Arguments)
	if err != nil {
		return lib.ErrorResult(err)
	}

	_, err = service.Socket.SimpleCall("nano.set", input.Colors, nil)
	if err != nil {
		return lib.ErrorResult(err)
	}

	return client.InvokeResult{}
}

func getNanoInput(args wamp.List) (*nanoInput, error) {

	if len(args) < 1 {
		return nil, errors.New("Missing color")
	}
	colors := wamp.List{}
	for _, color := range args {
		colors = append(colors, color.(string))
	}
	return &nanoInput{Colors: colors}, nil
}
